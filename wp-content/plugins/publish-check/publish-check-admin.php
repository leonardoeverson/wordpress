<?php

	function publish_check_admin(){
		?>
		<!-- Latest compiled and minified CSS -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	  <!-- Optional theme -->
	  <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.2.4.js"></script>
	  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.8/jquery.mask.js"></script>
	  <div class="wrap">
	    <form class="form-horizontal" action="../wp-content/plugins/publish-check/grava_tag.php" method="POST">
	      <fieldset>

	        <!-- Form Name -->
	        <legend>Cadastro de Missão</legend>
			
			<button class="btn btn-default" type="button">Listar Missões ativas</button>
	        <!-- Text input-->
	        <div class="form-group">
	          <label class="col-md-4 control-label" for="nome">Nome da Missão</label>  
	          <div class="col-md-4">
	            <input id="nome" name="nome" placeholder="nome da missão" class="form-control input-md" required="" type="text">

	        </div>
	    </div>

	    <!-- Text input-->
	    <div class="form-group">
	      <label class="col-md-4 control-label" for="tag">Tag</label>  
	      <div class="col-md-4">
	        <input id="tag" name="tag" placeholder="tag" class="form-control input-md" required="" type="text">

	    </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	  <label class="col-md-4 control-label" for=data_inicio">Prazo Inicial</label>  
	  <div class="col-md-4">
	    <input id="data_inicio" name="data_inicio" placeholder="prazo inicial" class="form-control input-md" required="" type="text">

	</div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	  <label class="col-md-4 control-label" for="data_final">Prazo Final</label>  
	  <div class="col-md-4">
	    <input id="data_final" name="data_final" placeholder="prazo final" class="form-control input-md" required="" type="text">

	</div>
	</div>
	<!-- Textarea -->
	<div class="form-group">
	  <label class="col-md-4 control-label" for="descricao">Descrição</label>
	  <div class="col-md-4">                     
	    <textarea class="form-control" id="descricao" name="descricao"></textarea>
	</div>
	</div>
	<!-- Button -->
	<div class="form-group">
	  <div class="col-md-4">
	    <button id="Enviar" name="Enviar" class="btn btn-primary">Gravar Missão</button>
	</div>
	</div>
	</fieldset>
	</form>
	<script type="text/javascript">
	  $('#data_inicio').mask('00/00/0000');
	  $('#data_final').mask('00/00/0000');
	</script>

	</div>
		<?php
	}

?>