<?php
/* Plugin Name: Publish Hook file
Description: Plugin for extra theme independent code
*/

require 'pontuacao.php';
require 'publish-check-admin.php';
require 'lista_missao.php';

/*-- Alterando a publicação de posts--*/
//Desativa comportamento padrão de permitir posts
function return_publish_permissions() {
	$user = get_role( 'author' );
	$user->add_cap( 'publish_posts' );
}
register_deactivation_hook( __FILE__, 'return_publish_permissions' );

//Ativa o comportamento de limitação de publicação de posts
function take_away_publish_permissions() {
	$user = get_role( 'author' );
	$user->add_cap('publish_posts',false);
}
register_activation_hook( __FILE__, 'take_away_publish_permissions' );

//Adiciona a tela de cadastro de missão e o submenu de desativar missão
add_action( 'admin_menu', 'my_admin_menu' );
function my_admin_menu() {
	add_menu_page( 'Cadastro de Missão', 'Cadastro de Missão', 'manage_options', 'publish-check/publish-check.php', 'publish_check_admin', 'dashicons-tickets', 6  );
	add_submenu_page(__FILE__, 'Desativar missão', 'Desativar missão', 'manage_options', __FILE__.'/lista_missao.php', 'lista_missao');
}

//Ativa a tela de cadastro de pontuação
add_action( 'admin_menu', 'pontuacao_por_post' );
function pontuacao_por_post(){
    add_menu_page( 'Pontuação por Post', 'Pontuação por Post', 'manage_options', 'publish-check/pontuacao.php', 'pontuacao', 'dashicons-tickets', 6  );

}


?>