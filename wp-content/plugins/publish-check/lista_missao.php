<?php

require_once 'conexao.php';

function lista_missao(){	
	$conexao = Conexao::db();
	$retq = $conexao->query("SELECT es_terms.term_id, es_terms.name, es_term_taxonomy.description FROM es_term_taxonomy,es_terms where es_terms.term_id = es_term_taxonomy.term_id and es_term_taxonomy.taxonomy = 'post_tag' and ativo = 1");
	$result_query = $retq->fetch_all(MYSQLI_ASSOC);

	echo '<legend><h1>Listar Missões Ativas</h1></legend>';
	
   ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.2.4.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.8/jquery.mask.js"></script>
    <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<div class=" col-xs-6 col-md-2">
     <div class="form-group">
      <label for="selecao" class="col-form-label">Missões ativas</label>
      <select name="selecao" class="select_evento evento form-control input-sm" id="tipo">

      <?php

       foreach ($result_query as $chave => $valor) {			     		  		
			echo '<option value='.$valor['term_id'].'>'.$valor['name'].'</option>'; 				     	
		}
       ?>
      
   </select>
   <br>
   <br>
   <button type="submit" id="desativa_missao" class="btn btn-danger">Desativar Missão</button>
   <script type="text/javascript">
   		$('#desativa_missao').click(function(event) {
   		$('#tipo')[0].value
   		$.ajax({
   			url: '../wp-content/plugins/publish-check/desativa_missao.php',
   			type: 'POST',
   			dataType: 'html',
   			data: 'termo='+ $('#tipo')[0].value,
   		})
   		.done(function(mensagem) {
   			alert(mensagem);
   			console.log("success");
   			location.reload(true);
   		})
   		.fail(function() {
   			console.log("error");
   		})
   		.always(function() {
   			console.log("complete");
   		});
   		
   		});
   </script>
	<?php
}
?>