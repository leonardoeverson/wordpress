<?php
/**
 * The template for displaying all pages.
 *
 * @package Grap
 */
require_once './wp-content/plugins/publish-check/conexao.php';
$conn = Conexao::db();
get_header(); 
?>
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.js"></script>
<div id="primary" class="content-area">
	<main id="main" class="site-main">


		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'page' ); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
			//	comments_template();
			//endif;
				//Criar tabela de pontuação
			if(is_page('dados-gerais') || is_page('5')){
				if(is_user_logged_in() == 1){
					?>
					<div class="container">
						<div id="dados_equipes" class="row" style="margin-top: -100px;">
								<legend><h2>Pontuações</h2></legend>
								<canvas id="canvas_" width="400" height="200"></canvas>
								<script type="text/javascript">
									label = '';
									quantidade = '';
								</script>
								<?php
								
								$result =$conn->query("select es_users.user_login as nome, sum(pontuacao.pontuacao) as quantidade from pontuacao, es_posts, es_users where pontuacao.user_id = es_users.ID and pontuacao.post_id = es_posts.ID group by es_users.user_login");

								$return_ = $result->fetch_all(MYSQLI_ASSOC);
								?>
								<script type="text/javascript">

									<?php
									//As variáveis recebem os dados vindos do php para inserção
									//no gráfico de pontuação
									foreach ($return_ as $chave) {
										?>
										label += <?php echo "'".$chave['nome'].",'";?>;
										quantidade += <?php echo "'".$chave['quantidade'].",'";?>;

										<?php
									}
									?>
									//Preparação para antes de carregar os dados no gráfico
									label = label.split(',');
									quantidade = quantidade.split(',');
									label.pop();
									quantidade.pop();
									//String to INT
									for (var i = 0; i < quantidade.length; i++) {
										quantidade[i] = parseInt(quantidade[i]);
									}

									ctx = document.getElementById('canvas_')
									//gráfico de barras
									var myBarChart = new Chart(ctx, {
										type: 'bar',
										data: {
											labels: label,
											datasets: [
											{
												label: "Gráfico de pontuação por equipes",
												backgroundColor: [
												'rgba(255, 99, 132, 0.2)'
												],
												borderColor: [
												'rgba(255,99,132,1)'
												],
												borderWidth: 1,
												data: quantidade,
											}
											]
										},
										options: {
											scales: {
												yAxes: [{
													ticks: {
														beginAtZero:true
													}
												}]
											}
										}
									});
								</script>
								
						</div>
						<div class="row">
							<div class="col-sm-9"></div>
						</div>
						<div id="missoes" class="row" style="margin-top: 20px">
							<legend><h2>Missões Ativas</h2></legend>
							<div class="col-sm-9">
								<?php
				     		  	//Missões Ativas
								$result = $conn->query("SELECT es_terms.name, es_term_taxonomy.description FROM es_term_taxonomy,es_terms where es_terms.term_id = es_term_taxonomy.term_id and es_term_taxonomy.taxonomy = 'post_tag' and ativo = 1") or die();
								$return_ = $result->fetch_all(MYSQLI_ASSOC);
								foreach ($return_ as $chave => $valor) {
									echo '<b>Tag: '.$valor['name'].'</b><br>';				     		  		
									echo '<label>Descrição da Missão: </label>'.$valor['description'].'<br>';				     	
								}

								?>
							</div>
						</div>
					</div>
					<?php
				}else{
					echo 'O usuário precisa se logar para ver o conteúdo';
				} 
			}  
			?>

		<?php endwhile; // End of the loop. ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
