<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
	
<head>
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="description" content="<?php bloginfo( 'description' ); ?>" />
    <meta http-equiv="content-type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
    <link rel="profile" href="http://gmpg.org/xfn/11" />
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>	
	<?php wp_head(); ?>   
</head>

<body <?php body_class(); ?>>

<div class="hide">
	<p><a href="#content"><?php _e( 'Skip to content', 'simplifiedblog' ); ?></a></p>
</div>

<div class="tlo">

<div id="logo"> 

		<?php if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {   
            the_custom_logo();
        } else { ?>          
            <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
                <?php $description = get_bloginfo( 'description', 'display' );
                if ( $description || is_customize_preview() ) { ?>
                    <p class="site-description"><?php echo esc_attr( $description );?></p> 
        <?php } } ?>
      	
</div>
<div class="clear"></div>



<?php
if ( has_nav_menu( 'primary' ) ) {
/*Only if there is menu in primary location*/
?>
<div id="menutoggle"><a href="javascript:toggleByClass('hidder-99');"><span class="fa fa-ellipsis-h"></span></a></div>
<?php
  wp_nav_menu( array(
    'theme_location'    => 'primary',
    'depth'             => '2',
	'container'       => 'div',
	'container_class' => '',
	'container_id'    => 'menuline',
	'menu_class'      => 'menu hidder-99',
	'menu_id'         => '',
	'echo'            => true,
	'fallback_cb'     => '',
	'items_wrap'      => '<nav><ul id="%1$s" class="%2$s">%3$s</ul></nav>',
	'walker'          => ''
  ));
};
  ?>