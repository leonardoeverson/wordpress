<div id="footer">

<div class="sidebar">
		<?php dynamic_sidebar( 'Footer' ); ?>
</div>


<div class="copy"><?php printf( __( '&copy; %s', 'simplifiedblog' ), date_i18n('Y') ) ?> - <?php simplifiedblog_footer(); ?></div>

</div>
<?php wp_footer(); /* this is used by many Wordpress features and for plugins to work properly */ ?>
</div><!--.tlo-->

</body>
</html>