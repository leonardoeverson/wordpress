��    P      �  k         �  
   �     �     �     �  	   �                           (     �  G  ,   G
  %   t
  #   �
  #   �
     �
     �
     �
  	                  3     N  "   U     x     �     �     �     �     �     �     �          $     1     @     O     h     �     �     �     �  $   �  ?     W   C     �     �     �     �     �     �     �     �     �  
             &     <  	   C     M     [     `  ;   g  ;   �  ?   �          ,     2     @     S     f  \   v  
   �     �     �            T   *  K       �     �     �     �                              $  *   (  �  S  ,   S  %   �  #   �  #   �     �     �               -  !   2     T  	   o      y     �     �  
   �     �     �     �          +     K     j     �     �     �     �     �     �       
     +     R   H  U   �     �     �            	   (     2     7     H     [  
   i     t     �     �     �     �     �     �  d   �  h   A  S   �     �               $     1     =  T   Q  	   �     �     �     �     �  E        ?   @       
          *   -   .      '           F   O                        +   K      %   L   I             2   B       &             #              (       )   !      <   J      7   9   G   E       C      P   5      	       0   $                H      :             A   6   8              N                    1                         M   ,          >   D   /       "   =   3   4       ;    % Comments &gt;&gt; &larr; Older Comments &lt;&lt; 1 Comment 1280 1400 1600 1920 980 <div class="more">Read More &rarr;</div> <p>
			  Do you like Simplified theme? You can update to Simplified Pro to support the developer and get even more exciting features:</p>
				<ul style="font-weight:bold;padding-left:10px;">
				<li>Premium widget pack. Pixel perfect and designed to look great with your theme.</li>
				<li>Breadcrumbs navigation support</li>
				<li>30+ Google fonts</li>
				<li>Featured image support for posts and pages</li>
				<li>Lifetime updates for Simplified Pro theme</li>
				<li>Premium support for 1 year!</li>			
				</ul>		  
			  <h2 style="padding-left:10px;">
			  <a href="http://www.poisonedcoffee.com/simplifiedpro/">Read More</a>
			  </h2>
			  <h2 style="padding-left:10px;"><a href="http://www.poisonedcoffee.com/forums/">Support forums</a>
			  </h2>
			   <span class="fa fa-folder-open-o"></span> %s <span class="fa fa-search"></span> %s <span class="fa fa-tags"></span> %s <span class="fa fa-user"></span> %s Archives Blog layout Blogging area Color Body Font Bold Bold or normal headings? Both single and blog views Center Choose max site width (in pixels). Comments are closed. Daily Archives: %s Edit Font Options Footer Copyright Footer sidebar Full Width / No sidebar HIDE ALL meta info for pages HIDE ALL meta info for posts Heading Font Heading weight Headings Color Hide author in post meta Hide categories in post meta Hide comment link in post meta Hide date in post meta Hide tags in post meta Hover Color If checked - no meta info for pages. It looks like nothing was found over here. Maybe try searching? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Layout Options Leave a comment Left Left Sidebar Links Color Logo Logo alignment Main sidebar Meta Options Misc Color Monthly Archives: %s Newer Comments &rarr; Normal Not Found Nothing Found Page Pages: Pick a font for body text (save and press F5 to see changes Pick a font for headings (save and press F5 to see changes) Post meta, menu items and some other interface elements use it. Primary Menu Right Right Sidebar Single page layout Single post layout Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Text Color Upgrade to Simplified PRO Where will the logo be? Width Yearly Archives: %s comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-30 12:49+0100
PO-Revision-Date: 2016-01-30 23:26+0100
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.6
Language-Team: 
Last-Translator: 
Language: de
 % Kommentare &gt;&gt; &larr; Ältere Kommentare &lt;&lt; 1 Kommentar 1280 1400 1600 1920 980 <div class="more">Weiterlesen &rarr;</div> <p>
			  Do you like Simplified theme? You can update to Simplified Pro to support the developer and get even more exciting features:</p>
				<ul style="font-weight:bold;padding-left:10px;">
				<li>Premium widget pack. Pixel perfect and designed to look great with your theme.</li>
				<li>Breadcrumbs navigation support</li>
				<li>30+ Google fonts</li>
				<li>Featured image support for posts and pages</li>
				<li>Lifetime updates for Simplified Pro theme</li>
				<li>Premium support for 1 year!</li>			
				</ul>		  
			  <h2 style="padding-left:10px;">
			  <a href="http://www.poisonedcoffee.com/simplifiedpro/">Read More</a>
			  </h2>
			  <h2 style="padding-left:10px;"><a href="http://www.poisonedcoffee.com/forums/">Support forums</a>
			  </h2>
			   <span class="fa fa-folder-open-o"></span> %s <span class="fa fa-search"></span> %s <span class="fa fa-tags"></span> %s <span class="fa fa-user"></span> %s Archive Blog-Layout Artikel Hintergrundfarbe Generelle Schrift Fett Überschriften: fett oder normal? Einzel- und Blog-Ansichten Zentriert Maximale Website Breite in Pixel Kommentare sind geschlossen. Tagesarchiv: %s Bearbeiten Schrift-Optionen Footer Urheberrecht Fußzeilen Seitenleiste Volle Breite / Kein Sidebar ALLE Seite Metadaten ausblenden ALLE Post Metadaten ausblenden Schrift für Überschriften Überschrift Fett Überschriften Farbe Autor ausblenden Kategorien ausblenden Kommentar Nummer ausblenden Datum ausblenden Tags ausblenden Hoverfarbe Wenn markiert - keine Metadaten für Seiten Wir konnten den gesuchten Inhalt nicht finden. Probieren Sie die Suchfunktion aus! Es scheint, dass wir dir nicht weiterhelfen können. Möglicherweise hilft die Suche. Layout Hinterlasse einen Kommentar Links Sidebar links Linkfarbe Logo Logo Ausrichtung Haupt Seitenleiste Meta-Optionen Misc Farbe Monatsarchiv: %s Neuere Kommentare &rarr; Normal Nicht gefunden Es wurde nichts gefunden. Seite Seiten: Wählen Sie eine Schriftart für Fließtext (Speichern und drücken Sie F5, um Änderungen zu sehen) Wählen Sie eine Schriftart für Überschriften (Speichern und drücken Sie F5, um Änderungen zu sehen) Metadaten, Menüpunkte und einige andere Oberflächenelemente verwenden diese Farbe Primäres Menü Rechts Sidebar rechts Seite-Layout Post-Layout Zum Inhalt springen Es gibt keine passenden Suchergebnisse. Bitte versuche es mit anderen Suchbegriffen. Textfarbe Upgrade to Simplified PRO Horizontale Ausrichtung Website Breite Jahresarchiv: %s Ein Gedanke zu &bdquo;%2$s&ldquo; %1$s Gedanken zu &bdquo;%2$s&ldquo; 