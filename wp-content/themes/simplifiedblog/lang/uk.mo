��    P      �  k         �  
   �     �     �     �  	   �                           (     �  G  ,   G
  %   t
  #   �
  #   �
     �
     �
     �
  	                  3     N  "   U     x     �     �     �     �     �     �     �          $     1     @     O     h     �     �     �     �  $   �  ?     W   C     �     �     �     �     �     �     �     �     �  
             &     <  	   C     M     [     `  ;   g  ;   �  ?   �          ,     2     @     S     f  \   v  
   �     �     �            T   *  �       A     Y  (   b     �     �     �     �     �     �     �  4   �  �  �  ,   �  %     #   ;  #   _     �     �  "   �     �  
   �  6   �  U   )       T   �  )   �          /  
   D     O  +   `     �  G   �  C   �     0  %   P     v     �  !   �  !   �  ,   �     #  5   ;  [   q  Z   �  �   (     �  !   �  
   �     �          -  )   <     f     �     �     �  &   �            "   0     S     d  �   v  �   	  �   �           2      ?      [      y   $   �   l   �      %!  &   =!     d!     �!     �!  �   �!     ?   @       
          *   -   .      '           F   O                        +   K      %   L   I             2   B       &             #              (       )   !      <   J      7   9   G   E       C      P   5      	       0   $                H      :             A   6   8              N                    1                         M   ,          >   D   /       "   =   3   4       ;    % Comments &gt;&gt; &larr; Older Comments &lt;&lt; 1 Comment 1280 1400 1600 1920 980 <div class="more">Read More &rarr;</div> <p>
			  Do you like Simplified theme? You can update to Simplified Pro to support the developer and get even more exciting features:</p>
				<ul style="font-weight:bold;padding-left:10px;">
				<li>Premium widget pack. Pixel perfect and designed to look great with your theme.</li>
				<li>Breadcrumbs navigation support</li>
				<li>30+ Google fonts</li>
				<li>Featured image support for posts and pages</li>
				<li>Lifetime updates for Simplified Pro theme</li>
				<li>Premium support for 1 year!</li>			
				</ul>		  
			  <h2 style="padding-left:10px;">
			  <a href="http://www.poisonedcoffee.com/simplifiedpro/">Read More</a>
			  </h2>
			  <h2 style="padding-left:10px;"><a href="http://www.poisonedcoffee.com/forums/">Support forums</a>
			  </h2>
			   <span class="fa fa-folder-open-o"></span> %s <span class="fa fa-search"></span> %s <span class="fa fa-tags"></span> %s <span class="fa fa-user"></span> %s Archives Blog layout Blogging area Color Body Font Bold Bold or normal headings? Both single and blog views Center Choose max site width (in pixels). Comments are closed. Daily Archives: %s Edit Font Options Footer Copyright Footer sidebar Full Width / No sidebar HIDE ALL meta info for pages HIDE ALL meta info for posts Heading Font Heading weight Headings Color Hide author in post meta Hide categories in post meta Hide comment link in post meta Hide date in post meta Hide tags in post meta Hover Color If checked - no meta info for pages. It looks like nothing was found over here. Maybe try searching? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Layout Options Leave a comment Left Left Sidebar Links Color Logo Logo alignment Main sidebar Meta Options Misc Color Monthly Archives: %s Newer Comments &rarr; Normal Not Found Nothing Found Page Pages: Pick a font for body text (save and press F5 to see changes Pick a font for headings (save and press F5 to see changes) Post meta, menu items and some other interface elements use it. Primary Menu Right Right Sidebar Single page layout Single post layout Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Text Color Upgrade to Simplified PRO Where will the logo be? Width Yearly Archives: %s comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-01-30 12:49+0100
PO-Revision-Date: 2016-01-30 23:25+0100
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.8.6
Last-Translator: 
Language-Team: poisonedcoffee.com <mail@poisonedcoffee.com>
Language: uk
 Коментарів: % &gt;&gt; &larr; Старіші коментарі &lt;&lt; 1 Коментар 1280 1400 1600 1920 980 <div class="more">Читати далі &rarr;</div> <p>
			  Вам подобається тема Simplified? Якщо так, то купіть Simplified Pro, щоб підтримати розробника і отримати ще більше функцій:</p>
				<ul style="font-weight:bold;padding-left:10px;">
				<li>Додаткові віджети. Адаптовані до вашої теми.</li>
				<li>Breadcrumbs навігація</li>
				<li>30+ шрифтів від Google</li>
				<li>Підтримка featured image для постів і сторінок</li>
				<li>Безкоштовні пожиттєві оновлення для Simplified Pro</li>
				<li>Один рік підтримки через форум і email!</li>			
				</ul>		  
			  <h2 style="padding-left:10px;">
			  <a href="http://www.poisonedcoffee.com/simplifiedpro/">Читайте більше!</a>
			  </h2>
			  <h2 style="padding-left:10px;"><a href="http://www.poisonedcoffee.com/forums/">Форум підтримки</a>
			  </h2>
			   <span class="fa fa-folder-open-o"></span> %s <span class="fa fa-search"></span> %s <span class="fa fa-tags"></span> %s <span class="fa fa-user"></span> %s Архіви Формат блогу Колір фону дописів Основний шрифт Жирні Жирні чи нормальні заголовки? Коли переглядаєте сторінку/пост/список постів Посередині Оберіть максимальну ширину сайту (в пікселях). Коментування вимкнено Денні архіви: %s Редагувати Шрифт Копірайт Сайдбар в підвалі сайту Без сайдбара Приховати ВСЮ метаінформацію сторінок Приховати ВСЮ метаінформацію постів Шрифт заголовків Жирність заголовків Колір заголовків Сховати автора Сховати категорії Сховати коментарі Сховати дату публікації Сховати теги Колір посилань при наведенні Якщо вибрано: ніякої метаінформації про сторінки. Схоже, що тут нічого не знайдено. Спробуйте пошук? Здається ми не можемо знайти того, що ви шукаєте. Скористайтесь пошуком. Формат сторінок Залишити коментар Зліва Сайдбар зліва Колір посилань Логотип Вирівнювання логотипу Основний сайдбар Мета інформація Додатковий колір Архів місяця: %s Новіші коментарі &rarr; Нормальні Не знайдено Нічого не знайдено Сторінка Сторінки: Оберіть шрифт для основного тексту (збережіть і натисніть F5, щоб побачити зміни) Оберіть шрифт для заголовків (збережіть і натисніть F5, щоб побачити зміни) Колір метаінформації, елементів меню і деяких інших елементів інтерфейсу. Головне меню Справа Сайдбар справа Формат сторінок Формат постів Перейти до контенту За вашим запитом нічого не знайдено. Спробуйте інший запит. Колір тексту Оновитися до Simplified PRO Де буде логотип? Ширина сайту Архів року: %s %1$s коментар щодо &ldquo;%2$s&rdquo; %1$s коментаря щодо &ldquo;%2$s&rdquo; %1$s коментарів щодо &ldquo;%2$s&rdquo; 